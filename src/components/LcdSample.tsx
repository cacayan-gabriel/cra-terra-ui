import React from 'react'
// import fetch from 'isomorphic-fetch';
const fetch = require('isomorphic-fetch');
import {
  MsgSend,
  MnemonicKey,
  Coins,
  LCDClient
} from '@terra-money/terra.js';

const LcdSample = () => {

  (async () => {
    // Fetch gas prices and convert to `Coin` format.
    const gasPrices = await (await fetch('https://bombay-fcd.terra.dev/v1/txs/gas_prices')).json();
    const gasPricesCoins = new Coins(gasPrices);
   
    const lcd = new LCDClient({
      URL: "https://bombay-lcd.terra.dev/",
      chainID: "bombay-12",
      gasPrices: gasPricesCoins,
      gasAdjustment: "1.5",
      // gas: 10000000,
    });
  
    // console.log(lcd);
    // Replace with address to check.
    const address = 'terra16ukgundyaxpqry04tsfsczaw3fwvd92tapqssv';
    const [balance] = await lcd.bank.balance(address);
    // console.log(balance.toData().filter(x => x.denom == 'uusd'));

    })();

    return (<div>LcdSample</div>)

    }

    export default LcdSample