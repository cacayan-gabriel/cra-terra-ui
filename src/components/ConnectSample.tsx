import { useWallet, WalletStatus } from '@terra-money/wallet-provider';
import React from 'react';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import SignalWifiStatusbar4BarOutlinedIcon from '@mui/icons-material/SignalWifiStatusbar4BarOutlined';
import FmdGoodOutlinedIcon from '@mui/icons-material/FmdGoodOutlined';
import Button from '@mui/material/Button';

export function ConnectSample() {
  const {
    status,
    network,
    wallets,
    availableConnectTypes,
    availableInstallTypes,
    availableConnections,
    supportFeatures,
    connect,
    install,
    disconnect,
  } = useWallet();

  // console.log(availableConnectTypes[0]);

  return (
    <Card variant="outlined" sx={{ width: 500, p: 4, mx: "auto", mt: 4}}>
      <Typography variant="h3" gutterBottom color="primary">
        Connect Sample
      </Typography>
      <section>
        <div style={{ display: "flex" }}>
          <SignalWifiStatusbar4BarOutlinedIcon color="primary" sx={{ mr: 2 }}></SignalWifiStatusbar4BarOutlinedIcon>
          <Typography variant="subtitle1" gutterBottom>
            Status
          </Typography>
        </div>
        <Typography variant="subtitle1" gutterBottom>
          { status == 'WALLET_CONNECTED' ? 'Wallet is connected.' : 'Wallet is not connected.'}
          </Typography>
        <div style={{ display: "flex" }}>
          <FmdGoodOutlinedIcon color="secondary" sx={{ mr: 2 }}></FmdGoodOutlinedIcon>
           <Typography variant="subtitle1" gutterBottom>
            Wallet Address
          </Typography>
        </div>
        <Typography variant="subtitle1" gutterBottom>
         { status == 'WALLET_CONNECTED' ? wallets[0].terraAddress : 'Wallet address not found.'}
        </Typography>
      </section>

      <footer>
        {status === WalletStatus.WALLET_NOT_CONNECTED && (
          <>
            {availableInstallTypes.map((connectType) => (
              <button
                key={'install-' + connectType}
                onClick={() => install(connectType)}
              >
                Install {connectType}
              </button>
            ))}
            {/* {availableConnectTypes.map((connectType) => (
              // <button
              //   key={'connect-' + connectType}
              //   onClick={() => connect(connectType)}
              // >
              //   Connect {connectType}
              // </button>
              <Button variant="outlined"  key={'connect-' + connectType}
                onClick={() => connect(connectType)}>Connect {connectType}</Button>
            ))} */}
            {
               <Button variant="outlined"  key={'connect-' + availableConnectTypes[0]}
                onClick={() => connect(availableConnectTypes[0])}>Connect {availableConnectTypes[0]}</Button>
            }
            <br />
            {/* {availableConnections.map(
              ({ type, name, icon, identifier = '' }) => (
                <button
                  key={'connection-' + type + identifier}
                  onClick={() => connect(type, identifier)}
                >
                  <img
                    src={icon}
                    alt={name}
                    style={{ width: '1em', height: '1em' }}
                  />
                  {name} [{identifier}]
                </button>
              ),
            )} */}
          </>
        )}
        {status === WalletStatus.WALLET_CONNECTED && (
          <Button variant="outlined" color="error" onClick={() => disconnect()}>Disconnect</Button>
        )}
      </footer>
    </Card>
  );
}
