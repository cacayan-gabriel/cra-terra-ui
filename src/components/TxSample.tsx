import { Fee, MsgSend, LCDClient, MnemonicKey, Coins } from '@terra-money/terra.js';
import {
  CreateTxFailed,
  Timeout,
  TxFailed,
  TxResult,
  TxUnspecifiedError,
  useConnectedWallet,
  UserDenied,
  useLCDClient 
} from '@terra-money/wallet-provider';
import React, { useCallback, useState, useEffect } from 'react';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import SignalWifiStatusbar4BarOutlinedIcon from '@mui/icons-material/SignalWifiStatusbar4BarOutlined';
import { bech32 } from 'bech32';
import axios from "axios"

const TEST_TO_ADDRESS = 'terra1t3z06eu78fc82r7uunmz33lrfnl8xuh48dj0tx';
// const TEST_TO_ADDRESS = 'terra12hnhh5vtyg5juqnzm43970nh4fw42pt27nw9g9';


// advanced address validation, it verify also the bech32 checksum
function isValid(address: any) {
  try {
    const { prefix: decodedPrefix } = bech32.decode(address); // throw error if checksum is invalid
    // verify address prefix
    return decodedPrefix === 'terra';
  } catch {
    // invalid checksum
    return false; 
  }
}

export function TxSample() {

  const [txResult, setTxResult] = useState<TxResult | null>(null);
  const [txError, setTxError] = useState<string | null>(null);
  const connectedWallet = useConnectedWallet();
  const lcd = useLCDClient(); // LCD stands for Light Client Daemon

  const proceed = useCallback(() => {

    if (!connectedWallet) {
      console.log('Wallet Not Connected');
      return;
    }

    if (connectedWallet.network.chainID.startsWith('columbus')) {
      alert(`Please only execute this example on Testnet`);
      return;
    }

    if(!isValid(TEST_TO_ADDRESS)) {
      console.log('Invalid Wallet Address');
      return;
    }

    setTxResult(null);
    setTxError(null);

    try {

      connectedWallet
      .post({
        fee: new Fee(1000000, '2000000uusd'),
        msgs: [
          new MsgSend(connectedWallet.walletAddress, TEST_TO_ADDRESS, {
            uusd: 1000000,
          }),
        ],
        memo: 'test from terra.js!',
      })
      .then((nextTxResult: TxResult) => {
        setTxResult(nextTxResult);
        // getTxInfo(nextTxResult);
        // console.log(lcd);
        // console.log(nextTxResult.result.txhash);
        })
      .catch((error: unknown) => {
        if (error instanceof UserDenied) {
          setTxError('User Denied');
        } else if (error instanceof CreateTxFailed) {
          setTxError('Create Tx Failed: ' + error.message);
        } else if (error instanceof TxFailed) {
          setTxError('Tx Failed: ' + error.message);
        } else if (error instanceof Timeout) {
          setTxError('Timeout');
        } else if (error instanceof TxUnspecifiedError) {
          setTxError('Unspecified Error: ' + error.message);
        } else {
          setTxError(
            'Unknown Error: ' +
              (error instanceof Error ? error.message : String(error)),
          );
        }
      });

    } catch(error: unknown) {
        if (error instanceof UserDenied) {
          setTxError('User Denied');
        } else if (error instanceof CreateTxFailed) {
          setTxError('Create Tx Failed: ' + error.message);
        } else if (error instanceof TxFailed) {
          setTxError('Tx Failed: ' + error.message);
        } else if (error instanceof Timeout) {
          setTxError('Timeout');
        } else if (error instanceof TxUnspecifiedError) {
          setTxError('Unspecified Error: ' + error.message);
        } else {
          setTxError(
            'Unknown Error: ' +
              (error instanceof Error ? error.message : String(error)),
          );
        }
      }

  }, [connectedWallet]);


  console.log(JSON.stringify(txResult, null, 4));

  return (
   <Card variant="outlined" sx={{ width: 500, p: 4, mx: "auto", mt: 4, overflow: 'auto'}}>
     
      <Typography variant="h3" gutterBottom color="primary">
        Tx Sample
      </Typography>

      {connectedWallet?.availablePost && !txResult && !txError && (
        <Button variant="outlined" onClick={proceed}>Send 1USD to {TEST_TO_ADDRESS}</Button>
      )}

      {txResult && (
        <>
          {/* <pre>{JSON.stringify(txResult, null, 2)}</pre> */}

          <Grid container spacing={2}>

            <Grid item xs={12}>
                 <Typography variant="subtitle1" gutterBottom>
                    Successfully sent to
                </Typography>
            </Grid>

            <Grid item xs={2}>
                <Typography variant="subtitle1" gutterBottom>
                  From
                </Typography>
            </Grid>

            <Grid item xs={10}>
                <Typography variant="subtitle1" gutterBottom>
                  My wallet address
                </Typography>
            </Grid>


            <Grid item xs={2}>
                <Typography variant="subtitle1" gutterBottom>
                  To
                </Typography>
            </Grid>

            <Grid item xs={10}>
                <Typography variant="subtitle1" gutterBottom>
                  {TEST_TO_ADDRESS}
                </Typography>
            </Grid>

             <Grid item xs={2}>
                <Typography variant="subtitle1" gutterBottom>
                  Amount
                </Typography>
            </Grid>

            <Grid item xs={10}>
                <Typography variant="subtitle1" gutterBottom>
                 {/* {txResult.fee?} */}
                 123123123
                </Typography>
            </Grid>

            <Grid item xs={2}>
                <Typography variant="subtitle1" gutterBottom>
                  Fee
                </Typography>
            </Grid>

            <Grid item xs={10}>
                <Typography variant="subtitle1" gutterBottom>
                 {txResult.fee?.gas_limit}
                </Typography>
            </Grid>

            <Grid item xs={2}>
                <Typography variant="subtitle1" gutterBottom>
                    Tx Hash:
                </Typography>
            </Grid>
             <Grid item xs={10}>
                <Typography variant="subtitle1" gutterBottom>
                   {txResult.result.txhash}
                </Typography>
            </Grid>
        
          </Grid>


          {connectedWallet && txResult && (
            <div>
              <a
                href={`https://finder.terra.money/${connectedWallet.network.chainID}/tx/${txResult.result.txhash}`}
                target="_blank"
                rel="noreferrer"
              >
                Open Tx Result in Terra Finder
              </a>
              <br />
            </div>
          )}
        </>
      )}

      {txError && <pre>{txError}</pre>}

      {(!!txResult || !!txError) && (
        <Button variant="contained" sx={{ mt: 4 }}
          onClick={() => {
            setTxResult(null);
            setTxError(null);
          }}
        >
          Clear result
        </Button>
      )}

          {!connectedWallet && <><div style={{ display: "flex" }}>
          <SignalWifiStatusbar4BarOutlinedIcon color="primary" sx={{ mr: 2 }}></SignalWifiStatusbar4BarOutlinedIcon>
          <Typography variant="subtitle1" gutterBottom>
            Status
          </Typography>
        </div>
        <p>Wallet is not connected!</p></>}

      {connectedWallet && !connectedWallet.availablePost && (
        <p>This connection does not support post()</p>
      )}

    </Card>
  );
}
