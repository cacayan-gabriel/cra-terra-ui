import { getChainOptions, WalletProvider } from '@terra-money/wallet-provider';
import { ConnectSample } from 'components/ConnectSample';
import { CW20TokensSample } from 'components/CW20TokensSample';
import { NetworkSample } from 'components/NetworkSample';
import { QuerySample } from 'components/QuerySample';
import { SignBytesSample } from 'components/SignBytesSample';
import { SignSample } from 'components/SignSample';
import { TxSample } from 'components/TxSample';
import  LcdSample  from 'components/LcdSample';
import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';
import '@fontsource/roboto/400.css';
import Typography from '@mui/material/Typography';

function App() {
  return (
    <main
      style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column', gap: 40, backgroundColor: '#dbeafe', overflow: 'auto' }}
    >
      <ConnectSample />
      {/* <QuerySample /> */}
      <TxSample />
      <LcdSample />
      {/* <SignSample />
      <SignBytesSample />
      <CW20TokensSample />
      <NetworkSample /> */}
    </main>
  );
}

getChainOptions().then((chainOptions) => {
  ReactDOM.render(
    <WalletProvider {...chainOptions}>
      <App />
    </WalletProvider>,
    document.getElementById('root'),
  );
});
